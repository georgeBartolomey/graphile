using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Remove", "GraphileFromCategory")]
    public class RemoveGraphileFromCategory : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public Graphile[] Files { get; set; }

        [Parameter]
        public string[] Categories { get; set; }
        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var categories = repository.Categories.Find(x => Categories.Contains(x.Name)).Select(x => x.Id);
            foreach (var file in Files)
            {
                foreach (var c in categories) file.Categories.Remove(c);
                repository.Files.Update(file);
            }
        }
    }
}