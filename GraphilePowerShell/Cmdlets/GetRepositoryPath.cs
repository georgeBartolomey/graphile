using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Get", "RepositoryPath")]
    public class GetRepositoryPath : PSCmdlet
    {



        protected override void ProcessRecord()
        {
            WriteObject(this.ProcessDirectory());
        }
    }
    public static class GetReposPathExt
    {
        public static string ProcessDirectory(this PSCmdlet cmdlet)
        {
            var dir = Process(cmdlet.SessionState.Path.CurrentLocation.Path);
            if (dir == null)
                throw new Exception("Current directory and its parents of current directory have no graphile init! To init use cmdlet Init-Graphile");
            Environment.CurrentDirectory = dir;
            return dir;
        }
        public static string Process(string curPath)
        {
            DirectoryInfo dir = new DirectoryInfo(curPath);
            while (dir.EnumerateFiles().FirstOrDefault(x => x.Name == "graphile.db") == null)
            {
                if (dir.Parent == null)
                    return null;
                dir = dir.Parent;
            }
            return dir.FullName;
        }
    }
}