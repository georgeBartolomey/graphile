using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Add", "FileToCategory")]
    public class AddFileToCategory : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public FileInfo File { get; set; }

        [Parameter]
        public string[] Categories { get; set; }

        [Parameter]
        public string Name { get; set; }
        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var categories = repository.Categories.Find(x => Categories.Contains(x.Name));
            
            if (categories.Count() == 0)
            {
                WriteWarning("Categories aren't exist!");
                WriteWarning("To create category use cmdlet New-Category");
                return;
            }
            if (!File.Exists)
            {
                WriteWarning("File isn't exist!");
                return;
            }

            if (string.IsNullOrEmpty(Name)) {
                Name = File.Name;
            }
            var model = new Graphile()
            {
                Id = Guid.NewGuid(),
                Name = Name,
                Categories = new List<Guid>(categories.Select(x => x.Id)),
                Extension = File.Extension
            };

            File.CopyTo(model.Id + File.Extension);
            repository.Files.Insert(model);
        }
    }
}