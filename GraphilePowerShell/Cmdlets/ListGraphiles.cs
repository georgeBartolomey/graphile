using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("List", "Graphiles")]
    public class ListGraphiles : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string[] Categories { get; set; }

        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            IEnumerable<Graphile> gfiles;
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            if (Categories != null && Categories.Length > 0)
            {
                var categories = Categories.SelectMany(x => repository.Categories.Find(y => Categories.Contains(y.Name)))
                .Distinct();
                var categoriesGuid = categories.Select(x => x.Id);
                gfiles = repository.Files.Find(x => x.Categories.Intersect(categoriesGuid).Count() > 0);
            }
            else gfiles = repository.Files.FindAll();

            var files = gfiles.Select(x => new GraphileView(repository) { Id = x.Id, Categories = x.Categories, Name = x.Name });
            foreach (var file in files)
            {
                WriteObject(file);
            }
        }
    }
}