using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("New", "Category")]
    public class NewCategory : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var exist = repository.Categories.FindOne(x => x.Name == Name);
            if (exist != null)
            {
                WriteWarning($"Category with equal name '{exist.Name}' is exist!");
                return;
            }
            Name = Name.ToLower();
            WriteObject(Name);
            repository.Categories.Insert(new Category() { Name = Name });
        }
    }
}