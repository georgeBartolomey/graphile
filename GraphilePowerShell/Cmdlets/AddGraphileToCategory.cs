using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Add", "GraphileToCategory")]
    public class AddGraphileToCategory : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string[] Files { get; set; }

        [Parameter]
        public string[] Categories { get; set; }
        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var categories = repository.Categories.Find(x => Categories.Contains(x.Name)).Select(x => x.Id);
            foreach (var file in repository.Files.Find(x => Files.Contains(x.Name)))
            {
                file.Categories.AddRange(categories);
                repository.Files.Update(file);
            }
        }
    }
}