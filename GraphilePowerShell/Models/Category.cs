using System;
using System.Collections.Generic;
using System.IO;
using LiteDB;

namespace GraphilePowerShell.Models
{
    public class Category
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public override string ToString() 
        {
            return Name;
        }
    }
}