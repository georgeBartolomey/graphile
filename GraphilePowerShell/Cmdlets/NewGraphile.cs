using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("New", "Graphile")]
    public class NewGraphile : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string Name { get; set; }

        [Parameter]
        public string[] Categories { get; set; }

        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var categories = repository.Categories.Find(x => Categories.Contains(x.Name));
            if (categories.Count() == 0)
            {
                WriteWarning("Categories aren't exist!");
                WriteWarning("To create category use cmdlet New-Category");
                return;
            }

            if (string.IsNullOrEmpty(Name))
                Name = Guid.NewGuid().ToString();

            var model = new Graphile()
            {
                Id = Guid.NewGuid(),
                Name = Name,
                Categories = new List<Guid>(categories.Select(x => x.Id)),
                Extension = Path.GetExtension(Name)
            };

            repository.Files.Insert(model);
            File.Create(model.Id + Path.GetExtension(Name));
        }
    }
}