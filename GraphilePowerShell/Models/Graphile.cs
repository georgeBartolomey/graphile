using System;
using System.Collections.Generic;
using LiteDB;
using System.Linq;
namespace GraphilePowerShell.Models
{
    public class Graphile
    {
        [BsonId]
        public Guid Id { get; set; }

        public List<Guid> Categories { get; set; }

        public string Name { get; set; }
        public string Extension { get; set; }
    }
    public class GraphileView : Graphile
    {
        Repository repository;
        public GraphileView(Repository repos)
        {
            repository = repos;
        }
        [BsonIgnore]
        public IEnumerable<string> CategoriesNames
        {
            get
            {
                return Categories.Select(x => repository.Categories.FindOne(y => y.Id == x)).Select(z => z.ToString());
            }
        }
    }
}