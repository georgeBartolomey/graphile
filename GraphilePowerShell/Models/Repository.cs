using LiteDB;
using System;
using System.IO;

namespace GraphilePowerShell.Models
{
    public class Repository
    {
        LiteDatabase database;
        public Repository(string directory)
        {
            database = new LiteDatabase($"{directory}{Path.DirectorySeparatorChar}graphile.db");
            database.Shrink();
        }

        public LiteCollection<Graphile> Files
        {
            get { return database.GetCollection<Graphile>("files"); }
        }

        public LiteCollection<Category> Categories
        {
            get { return database.GetCollection<Category>("categories"); }
        }
    }
}