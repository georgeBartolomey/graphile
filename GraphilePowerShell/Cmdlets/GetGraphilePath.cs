using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Get", "GraphilePath")]
    public class GetGraphilePath : PSCmdlet
    {
        [Parameter]
        public string Id { get; set; }
        [Parameter]
        public string Name { get; set; }
        [Parameter]
        public string[] Categories { get; set; }
        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(SessionState.Path.CurrentLocation.Path);

            IEnumerable<Graphile> files;
            if (Categories != null && Categories.Length > 0)
            {
                IEnumerable<Guid> categories =
                    Categories.Select(x => repository.Categories.FindOne(y => Categories.Contains(y.Name)))
                    .Select(x => x.Id);
                files = repository.Files.Find(x => x.Categories.Intersect(categories).Count() > 0);
                if (Name != null)
                    files = files.Where(x => x.Name == Name);
            }
            else
            {
                files = repository.Files.Find(x => x.Id.ToString() == Id).Take(1);
            }

            if (files.Count() == 0) {
                WriteWarning("Graphile not found!");
                return;
            }

            foreach (var model in files)
                WriteObject(Path.Combine(SessionState.Path.CurrentLocation.Path, model.Id + model.Extension));
        }
    }
}