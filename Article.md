# Категории вместо директорий. Инструмент для удобного хранение файлов

Вдохновившись [статьей](https://habr.com/ru/post/457328/) 
> "Категории вместо директорий, или Семантическая файловая система для Linux Vitis",

я решил сделать свой аналог утилиты `vitis` для PowerShell Core.

## Зачем я начал это делать

Во первых, `vitis` только для Linux.
Во вторых, хочется пользоваться "трубами" в PowerShell.

Так как я захотел сделать кроссплатформенную утилиту, я выбрал .Net Core.

## Предыстория

Сначала был хаос. Потом на диске появились папки.
Но хаос все еще царил. И появились теги, а также подтеги и синонимы тегов. Но хаос захватил и их. И были придуманы категории.

## Концепция

Файлы хранятся не в иерархии папок, а в "куче", с определенными категориями. Один файл может пренадлежать нескольким категориям.

Если нужно найти файл, удобнее ввести категории, которым он принадлежит. Это гараздо проще запомнить, чем в каких папках он хранится.

Но такая файловая система ни в коем случае не подходит для хранения кода, исходников и т. д.
Она предназначена для хранения, например, семейных фото, музыки, документов.

## Использование

Ну что же, приступим к демонстрации.

Установим Graphile:

```powershell
# install.ps1 находится в корневой папке с кодом
PS D:\Source\repos\Graphile> .\install.ps1
```

Сначала инициализируем Graphile в папке:

```powershell
# Импортируем установленный модуль
PS C:\Users\Dell\GraphileTest> Import-Module GraphilePowerShell
# Инициализируем Graphile в папке
PS C:\Users\Dell\GraphileTest> Init-Graphile
C:\Users\Dell\GraphileTest\graphile.db
```

Итак, мы все настроили. Теперь можно создать новую категорию:

```powershell
PS C:\Users\Dell\GraphileTest> New-Category -Name "music"
music
```

Добавим файлы:

```powershell
PS C:\Users\Dell\GraphileTest> Get-ChildItem "D:\Music\Pink Floyd - The Wall" -Recurse -Filter "*.mp3" | foreach { $_.FullName } | foreach {
>> Add-FileToCategory -File $_ -Categories "music"
>> }
```

Тем, кто не понял, что я сделал:

1. Получил список файлов в папке `Pink Floyd - The Wall`
2. Каждый файл преобразовал в полный путь к ним
3. Добавил каждый файл в категорию `music`

Теперь проверим, действительно ли мы добавили файлы в категорию:

```powershell
PS C:\Users\Dell\GraphileTest> List-Graphiles | Format-Table
```

Будет выведен список в формате:

```powershell
CategoriesNames Id Categories Name Extension
```

Добавим еще категорий и файлов:

```powershell
New-Category -Name "the-wall"
New-Category -Name "alan-parsons"

List-Graphiles -Categories "music" | foreach { Add-GraphileToCategory -Categories "the-wall" -Files $_.Name }

Get-ChildItem "D:\Music\The Alan Parsons Project - Turn of a Friendly Card" -File -Recurse | foreach { $_.FullName } | foreach { Add-FileToCategory -File $_ -Categories "music", "alan-parsons" }
```

Проверим файлы:

```powershell
List-Graphiles -Categories "alan-parsons" | Format-Table
```

Команда выведет все файлы, которые пренадлежат категории `alan-parsons`.

Больше файлов богу файловых систем!

```powershell
New-Category -Name "images"
Get-ChildItem "D:\Изображения" -File -Recurse | foreach { $.FullName } | foreach {
    Add-FileToCategory -File $_ -Categories "images"
}
```

Проверяем:

```powershell
List-Graphiles -Categories "images" | Format-Table
```

Все работает!

Но это только начало. Экспортируйте свою обычную файловую систему в Graphile и получите отличную организацию файлов.

## Откуда скачать Graphile

Вот [ссылка](https://gitlab.com/georgeBartolomey/graphile) на GitLab Graphile'а.

Лицензия `MIT`, так что можете использовать даже в коммерческих целях.

## Будущее проекта

Так как это PowerShell, а не хухры-мухры, можно сделать графическую оболочку под .NET Core.

Как это сделать:

1. Установить пакет Microsoft.PowerShell.SDK
2. Использовать класс PowerShell и вызывать Graphile.

Если вам понравился проект, делайте коммиты, `merge request`'ы и создавайте обсуждения.

Спасибо, за прочтение! Пользуйтесь и наслаждайтесь!
