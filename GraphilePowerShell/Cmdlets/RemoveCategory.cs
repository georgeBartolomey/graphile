using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Remove", "Category")]
    public class RemoveCategory : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string Name { get; set; }

        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            var category = repository.Categories.FindOne(x => x.Id.ToString() == Name | x.Name == Name);


            foreach (var file in repository.Files.Find(x => x.Categories.Contains(category.Id)))
            {
                file.Categories.Remove(category.Id);
                repository.Files.Update(file);
            }

            repository.Categories.Delete(x => x.Id.ToString() == Name | x.Name == Name);
        }
    }
}