using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Init", "Graphile")]
    public class InitGraphile : PSCmdlet
    {

        protected override void ProcessRecord()
        {
            Repository repository = new Repository(SessionState.Path.CurrentLocation.Path);
            repository.Files.FindAll();
            WriteObject($"{SessionState.Path.CurrentLocation.Path}{Path.DirectorySeparatorChar}graphile.db");
        }
    }
}