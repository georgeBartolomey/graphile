using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("Remove", "Graphile")]
    public class RemoveGraphile : PSCmdlet
    {
        [Parameter(ValueFromPipeline = true)]
        public string File { get; set; }

        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            repository.Files.Delete(x => x.Name == File);
        }
    }
}