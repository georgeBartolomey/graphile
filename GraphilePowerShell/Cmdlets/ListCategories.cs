using System;
using System.Management.Automation;
using GraphilePowerShell.Models;
using System.Linq;

namespace GraphilePowerShell.Cmdlets
{

    [Cmdlet("List", "Categories")]
    public class ListCategories : PSCmdlet
    {
        protected override void ProcessRecord()
        {
            this.ProcessDirectory();
            Repository repository = new Repository(System.IO.Directory.GetCurrentDirectory());
            foreach (var category in repository.Categories.FindAll()) {
                WriteObject(category);
            }
        }
    }
}